# Paysafe Payment Integration Demo App

#### Setup
* Clone the repository.
* `cd` into the directory.
* Install dependencies `npm install`

#### Development
* Start the server `npm start`