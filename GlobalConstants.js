class GlobalConstants {

    static currency = "USD"
    static locale = "en_US"
    static environment = "TEST"
    static companyName = "Example Paysafe Merchant"

    static stateCode = "CA"
    static countryCode = "US"
    static merchantRefNum = "jefnkwjenfk" // this should be dynamic for different merchants
    static merchantDescriptor = {
        dynamicDescriptor: "XYZ",
        phone: "1234567890"
    }
    static paymentMethods = ["card"]
    static customerIp = "172.0.0.1"

    static customerId = ""
    static merchantCustomerId = ""

}